package craky.componentc;

import java.util.Vector;

import javax.swing.ListModel;

public class JCScrollCheckBoxList extends JCScrollList
{
    private static final long serialVersionUID = -6886223334688958453L;

    public JCScrollCheckBoxList(JCCheckBoxList list)
    {
        super(list);
    }
    
    public JCScrollCheckBoxList()
    {
        this(new JCCheckBoxList());
    }
    
    public JCScrollCheckBoxList(Vector<?> listData)
    {
        this(new JCCheckBoxList(listData));
    }
    
    public JCScrollCheckBoxList(ListModel dataModel)
    {
        this(new JCCheckBoxList(dataModel));
    }
    
    public JCCheckBoxList getList()
    {
        return (JCCheckBoxList)list;
    }
}
