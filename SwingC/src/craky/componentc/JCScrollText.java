package craky.componentc;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.border.Border;

import craky.util.UIResourceManager;

public class JCScrollText extends JCScrollPane
{
    private static final long serialVersionUID = -8804672738482197370L;
    
    private static final Color NON_EDITABLE_BG = UIResourceManager.getColor(UIResourceManager.KEY_TEXT_NON_EDITABLE_BACKGROUND);
    
    private static final Color DISABLED_BG = UIResourceManager.getColor(UIResourceManager.KEY_TEXT_DISABLED_BACKGROUND);
    
    private JCTextArea editor;
    
    private Border normalBorder;
    
    private Border rolloverBorder;
    
    private Border nonEditableBorder;
    
    private Border nonEditableRolloverBorder;
    
    private Border disabledBorder;
    
    private MouseListener listener;
    
    private Color background;
    
    public JCScrollText(JCTextArea editor)
    {
        super();
        setViewportView(this.editor = editor);
        setBackground(UIResourceManager.getWhiteColor());
        setHeaderVisible(false);
        editor.clearBorderListener();
        editor.setBorder(UIResourceManager.getBorder(UIResourceManager.KEY_SCROLL_TEXT_BORDER));
        editor.setVisibleInsets(0, 0, 0, 0);
        editor.setAlpha(0.0f);
        normalBorder = new ImageBorder(UIResourceManager.getImageByName("border_normal.png"), 2, 2, 2, 2);
        rolloverBorder = UIResourceManager.getBorder(UIResourceManager.KEY_COMPOUND_TEXT_ROLLOVER_BORDER);
        nonEditableBorder = UIResourceManager.getBorder(UIResourceManager.KEY_COMPOUND_TEXT_NON_EDITABLE_BORDER);
        nonEditableRolloverBorder = UIResourceManager.getBorder(UIResourceManager.KEY_COMPOUND_TEXT_NON_EDITABLE_ROLLOVER_BORDER);
        disabledBorder = UIResourceManager.getBorder(UIResourceManager.KEY_COMPOUND_TEXT_DISABLED_BORDER);
        installListener();
        setBorder(normalBorder);
    }
    
    public JCScrollText()
    {
        this(new JCTextArea());
    }
    
    private void installListener()
    {
        listener = new MouseAdapter()
        {
            public void mouseEntered(MouseEvent e)
            {
                mouseIn();
            }

            public void mouseExited(MouseEvent e)
            {
                mouseOut();
            }
        };
        
        this.addMouseListener(listener);
        editor.addMouseListener(listener);
        viewport.addMouseListener(listener);
        horizontalScrollBar.addMouseListener(listener);
        verticalScrollBar.addMouseListener(listener);
        
        for(Component c: horizontalScrollBar.getComponents())
        {
            c.addMouseListener(listener);
        }
        
        for(Component c: verticalScrollBar.getComponents())
        {
            c.addMouseListener(listener);
        }
    }

    public JCTextArea getEditor()
    {
        return editor;
    }
    
    public void setEditable(boolean editable)
    {
        editor.setEditable(editable);
        mouseOut();
        
        if(isEnabled())
        {
            super.setBackground(editable? background: NON_EDITABLE_BG);
        }
    }
    
    public boolean isEditable()
    {
        return editor.isEditable();
    }
    
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        super.setBackground(enabled? (isEditable()? background: NON_EDITABLE_BG): DISABLED_BG);
        
        if(enabled)
        {
            mouseOut();
        }
        else if(normalBorder != null)
        {
            setBorder(disabledBorder);
        }
    }
    
    public void setBackground(Color bg)
    {
        background = bg;
        super.setBackground(bg);
    }
    
    private void mouseIn()
    {
        if(normalBorder != null && editor.isEnabled())
        {
            setBorder(editor.isEditable()? rolloverBorder: nonEditableRolloverBorder);
        }
    }
    
    private void mouseOut()
    {
        if(normalBorder != null && editor.isEnabled())
        {
            setBorder(editor.isEditable()? normalBorder: nonEditableBorder);
        }
    }
}