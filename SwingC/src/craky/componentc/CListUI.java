package craky.componentc;

import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicListUI;

import craky.util.UIUtil;

public class CListUI extends BasicListUI
{
    public static ComponentUI createUI(JComponent list)
    {
        return new CListUI();
    }

    public void update(Graphics g, JComponent c)
    {
        paintBackground(g, c);
        super.update(g, c);
    }
    
    private void paintBackground(Graphics g, JComponent c)
    {
        if(c instanceof JCList)
        {
            JCList list = (JCList)c;
            UIUtil.paintBackground(g, c, list.getBackground(), list.getBackground(), list.getImage(),
                            list.isImageOnly(), list.getAlpha(), list.getVisibleInsets());
        }
    }
    
    protected void installDefaults()
    {
        list.setLayout(null);
    }
    
    protected void uninstallDefaults()
    {
        if(list.getTransferHandler() instanceof UIResource)
        {
            list.setTransferHandler(null);
        }
    }
}