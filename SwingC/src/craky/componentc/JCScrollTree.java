package craky.componentc;

import java.awt.Color;

import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

import craky.util.UIResourceManager;

public class JCScrollTree extends JCScrollPane
{
    private static final long serialVersionUID = -9002172235348323323L;

    private static final Color DISABLED_BG = UIResourceManager.getColor(UIResourceManager.KEY_TEXT_DISABLED_BACKGROUND);

    private Border border;

    private Border disabledBorder;

    private Color background;
    
    private JCTree tree;
    
    public JCScrollTree(JCTree tree)
    {
        super();
        setViewportView(this.tree = tree);
        init();
    }
    
    public JCScrollTree()
    {
        this(new JCTree());
    }
    
    public JCScrollTree(TreeNode root)
    {
        this(new JCTree(root));
    }

    public JCScrollTree(TreeModel newModel)
    {
        this(new JCTree(newModel));
    }
    
    private void init()
    {
        setBorder(new LineBorder(new Color(84, 165, 213)));
        setDisabledBorder(new LineBorder(new Color(84, 165, 213, 128)));
        setBackground(UIResourceManager.getWhiteColor());
        setHeaderVisible(false);
        tree.setBorder(new EmptyBorder(0, 7, 0, 0));
        tree.setDisabledBorder(tree.getBorder());
        tree.setVisibleInsets(0, 0, 0, 0);
        tree.setAlpha(0.0f);
    }
    
    public Border getDisabledBorder()
    {
        return disabledBorder;
    }

    public void setDisabledBorder(Border disabledBorder)
    {
        this.disabledBorder = disabledBorder;
        
        if(!this.isEnabled())
        {
            super.setBorder(disabledBorder);
        }
    }
    
    public void setBorder(Border border)
    {
        this.border = border;
        super.setBorder(border);
    }
    
    public Color getDisabledForeground()
    {
        return tree.getDisabledForeground();
    }

    public void setDisabledForeground(Color disabledForeground)
    {
        tree.setDisabledForeground(disabledForeground);
    }
    
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        super.setBorder(enabled? border: disabledBorder);
        super.setBackground(enabled? background: DISABLED_BG);
    }
    
    public void setBackground(Color background)
    {
        this.background = background;
        super.setBackground(background);
    }
    
    public JCTree getTree()
    {
        return tree;
    }
}